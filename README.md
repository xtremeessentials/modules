# modules

### what are modules?
modules can be seen as extensions for xEssentials brought by the ideas and contributions from the community

### what are the rules?
most of the things are allowed however malicious code which could harm somebodies server, harvest passwords or does things as force ops these are not allowed in anyway.

### how can I publish my Module?
you can fork this project, then you can create a new folder and build your own project into this folder, you can request a Pull request and then one of our main developers will check the quality and safety of the code
before getting pulled and merged.

### does my module needs to follow up a license?
yes, xEssentials uses GPLv3 with MIT this means the same licenses of these modules need to be applied on the modules but you are free however to use your own GPLv3+MIT copyright yourself on the module aslong its compatible with xEssentials.
