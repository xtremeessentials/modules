package modules.villagers;

import java.io.File;
import java.util.Random;

import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitTask;

import tv.mineinthebox.essentials.helpers.Schematic;
import tv.mineinthebox.essentials.helpers.SchematicBuilder;

public class SchematicProvider {

	private final File[] schematics;
	private BukkitTask task;
	private final RealisticVillagers pl;
	
	public SchematicProvider(RealisticVillagers pl) {
		this.pl = pl;
		File dir = new File(pl.getDataFolder() + File.separator + "schematics");
		this.schematics = new File[dir.listFiles().length];
		int i = 0;
		for(File f : dir.listFiles()) {
			if(f.getName().endsWith(".schematic")) {
				this.schematics[i++] = f;
			}
		}
	}
	
	/**
	 * <p>returns the file of the schematic</p>
	 * 
	 * @author xize
	 * @param name - the name of the schematic
	 * @return File
	 */
	public File getSchematicByName(String name) {
		for(File f : schematics) {
			if(name.toLowerCase().startsWith(f.getName().toLowerCase())) {
				return f;
			}
		}
		
		return null;
	}
	
	
	/**
	 * <p>returns a random schematic name</p>
	 * 
	 * @author xize
	 * @return String
	 */
	public String getRandomSchematicName() {
		Random rand = new Random();
		if(schematics.length == 1) {
			return schematics[0].getName();
		}
		File schematic = schematics[rand.nextInt(schematics.length)];
		return schematic.getName();
	}
	
	public File[] getSchematics() {
		return this.schematics;
	}
	
	public void stop() {
		this.task.cancel();
	}
	
	public boolean isRunning() {
		if(this.task != null || !this.task.isCancelled()) {
			return true;
		}
		return false;
	}
	
	public void buildSchematic(Player p, Location loc, Schematic schematic) {
		SchematicBuilder builder = new SchematicBuilder(schematic, loc, p, pl.getEssentialsPlugin());
		builder.startGeneration(EntityType.VINDICATOR);
	}
	
}
