package modules.villagers;

import java.io.File;
import java.io.IOException;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import tv.mineinthebox.essentials.helpers.Schematic;
import tv.mineinthebox.essentials.helpers.SchematicUtil;
import tv.mineinthebox.essentials.module.command.ModuleCommandExecutor;
import tv.mineinthebox.essentials.module.plugin.ModulePlugin;

public class RealisticVillagers extends ModulePlugin implements ModuleCommandExecutor {

	@Override
	public void onEnable() {
		log("has been enabled!", ModuleLog.INFO);
		if(!isConfigGenerated()) {
			createConfiguration();
		}
		this.setExecutor("realisticvillagers", this);
	}

	@Override
	public void onDisable() {
		log("has been disabled!", ModuleLog.INFO);
	}

	public boolean isConfigGenerated() {
		File f = new File(getDataFolder() + File.separator + "config.yml");
		if(f.exists()) {
			return true;
		}
		return false;
	}

	public void createConfiguration() {
		File dir = new File(getDataFolder() + File.separator + "schematics");
		dir.mkdir();
		log("generating new configuration for "+getName()+"!", ModuleLog.INFO);
		File f = new File(getDataFolder() + File.separator + "config.yml");
		FileConfiguration con = YamlConfiguration.loadConfiguration(f);
		con.set("configuration.generationChance", 5);
		con.set("configuration.enableWARS", false);
		con.set("configuration.enabled", true);
		con.set("configuration.logcoords", true);
		try {
			con.save(f);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Object readConfig(String node) {
		File f = new File(getDataFolder() + File.separator + "config.yml");
		FileConfiguration con = YamlConfiguration.loadConfiguration(f);
		return con.get(node);
	}

	@Override
	public boolean execute(CommandSender sender, String cmd, String[] args) {
		if(cmd.equalsIgnoreCase("realisticvillagers")) {
			if(sender.hasPermission("realisticvillagers.command")) {
				if(args.length == 0) {
					sender.sendMessage(ChatColor.GOLD + ".:: RealisticVillagers ::..");
					sender.sendMessage(ChatColor.GRAY + "/realisticvillager :"+ ChatColor.WHITE + " shows help");
					sender.sendMessage(ChatColor.GRAY + "/realisticvillager test :"+ ChatColor.WHITE + " performs a module test");
				} else if(args.length == 1) {
					if(args[0].equalsIgnoreCase("test")) {
						if(sender instanceof Player) {
							Player p = (Player)sender;
							SchematicProvider provider = new SchematicProvider(this);
							if(provider.getSchematics().length > 0) {
								SchematicUtil schematicutil = new SchematicUtil(this.getEssentialsPlugin());
								Schematic schematic = schematicutil.loadSchematic(new File(getDataFolder() + File.separator + "schematics"+File.separator+provider.getRandomSchematicName()));
								provider.buildSchematic(p, p.getLocation(), schematic);
								sender.sendMessage(ChatColor.GREEN + "building!");
							} else {
								sender.sendMessage(ChatColor.RED + "you have no schematics installed!");
							}
						} else {
							sender.sendMessage(ChatColor.RED + "you need to be a player todo that!");
						}
					} else {
						sender.sendMessage(ChatColor.RED + "there are no schematics!");
					}
				} else {
					sender.sendMessage(ChatColor.RED + "sorry I don't know about this argument!");
				}
			} else {
				sender.sendMessage(ChatColor.RED + "you have no permission!");
			}
		}
		return false;
	}

}
