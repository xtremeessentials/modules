package modules.villagers;

import org.bukkit.scheduler.BukkitTask;

public class VillagePopulator extends Populator {

	private BukkitTask generator;
	private final RealisticVillagers pl;

	public VillagePopulator(RealisticVillagers pl) {
		this.pl = pl;
	}

	@Override
	public void startPopulator() {
		/*
		if(isRunning()) {
			return;
		} else {
			this.generator = new BukkitRunnable() {

				@Override
				public void run() {
					int chance = (int) pl.readConfig("configuration.generationChance");
					
					Random rand = new Random();
					int randomchance = rand.nextInt(100)+1;
					if(randomchance >= 100-chance) {
						//start to generate random village :)
						final SchematicProvider provider = new SchematicProvider(pl);
						final String randomschematic = provider.getRandomSchematicName();
						XPlayer[] players = pl.getEssentialsApi().getManagers().getPlayerManager().getPlayers();
						XPlayer p = players[rand.nextInt(players.length-1)];
						final Location loc = p.getLocation();
						
						ExecutorService service = Executors.newFixedThreadPool(1);
						Future<BlockState[]> fut = service.submit(new Callable<BlockState[]>() {

							@Override
							public BlockState[] call() {
								
								BlockState[] blocks = provider.pasteSchematic(randomschematic, loc);
								return blocks;
							}
							
						});
						if(fut.isDone()) {
							
						}
					}
				}

			}.runTaskTimer(pl.getEssentialsPlugin(), 1L, 800L);
		}
		*/
	}

	@Override
	public void stop() {
		if(isRunning()) {
			this.generator.cancel();
			this.generator = null;
		}
	}

	@Override
	public boolean isRunning() {
		return (this.generator != null && !this.generator.isCancelled());
	}

}
