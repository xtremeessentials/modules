package modules.villagers;

public abstract class Populator {
	
	/**
	 * <p>starts the populator!</p>
	 * 
	 * @author xize
	 */
	public abstract void startPopulator();
	
	/**
	 * <p>stops the populator!</p>
	 * 
	 * @author xize
	 */
	public abstract void stop();
	
	/**
	 * <p>returns either true if the task is cancelled or the task is null</p>
	 * 
	 * @author xize
	 * @return boolean
	 */
	public abstract boolean isRunning();
	

}
