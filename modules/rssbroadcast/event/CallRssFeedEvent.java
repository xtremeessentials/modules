package modules.rssbroadcast.event;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import modules.rssbroadcast.RssFeed;
import tv.mineinthebox.essentials.module.plugin.ModulePlugin;
import tv.mineinthebox.essentials.module.plugin.ModulePlugin.ModuleLog;

public class CallRssFeedEvent {
	
	private volatile ModulePlugin pl;
	private final FileConfiguration con;
	
	public CallRssFeedEvent(ModulePlugin pl, FileConfiguration con) {
		this.pl = pl;
		this.con = con;
	}

	private volatile RssFeed feed;
	private volatile boolean stop = false;
	
	/**
	 * @author xize
	 * @param starts the scheduler
	 */
	private void onChatSentRssBroadcast() {
		new BukkitRunnable() {
			protected boolean isItemFound = false;

			private URL url;

			private HttpURLConnection httpcon;
			private InputStreamReader input;
			private BufferedReader reader;

			@Override
			public void run() {
				try {
					if(stop) {
						saveLastFeed();
						cancel();
					}
					loadLastFeed();
					String author = null;
					String link = null;
					String title = null;
					this.isItemFound = false;
					this.url = new URL(con.getString("rss.use-rss-url"));
					this.httpcon = (HttpURLConnection) url.openConnection();
					this.httpcon.addRequestProperty("User-Agent", "Mozilla/4.76");
					this.httpcon.setUseCaches(false);
					this.input = new InputStreamReader(httpcon.getInputStream());
					this.reader = new BufferedReader(input);
					String line;
					String text = "";
					while((line = reader.readLine()) != null) {
						if(line.contains("<item>")) {
							isItemFound = true;
						}
						if(isItemFound) {
							if(line.contains("<title>")) {
								text = "";
								int firstPos = line.indexOf("<title>");
								String temp = line.substring(firstPos);
								temp = temp.replace("<title>", "");
								int lastPos = temp.indexOf("</title>");
								temp = temp.substring(0,lastPos);
								title = text+= temp;
							}
							if(line.contains("<link>")) {
								text = "";
								int firstPos = line.indexOf("<link>");
								String temp = line.substring(firstPos);
								temp = temp.replace("<link>", "");
								int lastPos = temp.indexOf("</link>");
								temp = temp.substring(0,lastPos);
								link = text+= temp;
							}
							if(line.contains("<author>")) {
								//first check if we have a email or something...
								text = "";
								int firstPos = line.indexOf("<author>");
								String temp = line.substring(firstPos);
								temp = temp.replace("<author>", "");
								int lastPos = temp.indexOf("</author>");
								temp = temp.substring(0,lastPos);
								author = text+= temp;
								//xenforo problem.
								if(author.contains("@")) {
									int space = author.indexOf(' ');
									author = author.substring(space+1);
								}
								
								break;
							}
						}
					}
					RssFeed afeed = new RssFeed(title, author.replaceAll("[^a-zA-Z0-9]", "").replace("(invalid@example.com)", ""), link);
					if(feed != null) {
						if(!feed.getTitle().equalsIgnoreCase(afeed.getTitle())) {
							feed = afeed;
							for(Player p : Bukkit.getOnlinePlayers()) {
								Bukkit.getPluginManager().callEvent(new RssFeedEvent(p, feed));        
							}        
						}
					} else {
						feed = afeed;
						for(Player p : Bukkit.getOnlinePlayers()) {
							Bukkit.getPluginManager().callEvent(new RssFeedEvent(p, feed));        
						}
					}
				} catch(NullPointerException e1) {
					pl.log("couldn't create a stored RssFeed object, probably because you don't have a propper connection at first run.", ModuleLog.WARNING);
				} catch (MalformedURLException e1) {
					pl.log("the url is wrong for the RSS!", ModuleLog.WARNING);
				} catch (IOException e1) {
					if(!e1.getMessage().contains("502")) {
						pl.log("connection timeout for the RSS event!", ModuleLog.WARNING);	
						if(con.getBoolean("rss.debug")) {
							pl.log("since the RSS timed out we want to be sure what is going on, error stack:", ModuleLog.DEBUG);
							e1.printStackTrace();
						}
					}
				} finally {
					try {
						reader.close();
						input.close();
						httpcon.disconnect();
					} catch(Exception e) {}
				}
			}

		}.runTaskTimerAsynchronously(pl.getEssentialsPlugin(), 100L, 2500L);
	}

	/**
	 * @author xize
	 * @param saves the last possible RssFeed
	 */
	public void saveLastFeed() {
		try {
			if(this.feed instanceof RssFeed) {
				File f = new File(pl.getDataFolder() + File.separator + "lastRssFeed.yml");
				YamlConfiguration con = YamlConfiguration.loadConfiguration(f);
				con.set("author", feed.getAuthor().replaceAll("[^a-zA-Z0-9]", ""));
				con.set("title", feed.getTitle());
				con.set("link", feed.getLink());
				con.save(f);
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @author xize
	 * @param loads the last possible RssFeed
	 */
	public void loadLastFeed() {
		try {
			File f = new File(pl.getDataFolder() + File.separator + "lastRssFeed.yml");
			if(f.exists()) {
				FileConfiguration con = YamlConfiguration.loadConfiguration(f);
				RssFeed rss = new RssFeed(con.getString("title"), con.getString("author").replaceAll("[^a-zA-Z0-9]", ""), con.getString("link"));
				this.feed = rss;
				f.delete();
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * @author xize
	 * @param returns true if the task is running otherwise false.
	 * @return Boolean
	 */
	public boolean isRunning() {
		return !stop;
	}
	
	/**
	 * @author xize
	 * @param cancels the task on the next tick.
	 */
	public void stop() {
		this.stop = true;
	}
	
	/**
	 * @author xize
	 * @param starts the task.
	 */
	public void start() {
		this.onChatSentRssBroadcast();
	}
}

