package modules.rssbroadcast;

import java.io.File;
import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import modules.rssbroadcast.event.CallRssFeedEvent;
import modules.rssbroadcast.listener.RssEventListener;
import tv.mineinthebox.essentials.module.command.ModuleCommandExecutor;
import tv.mineinthebox.essentials.module.plugin.ModulePlugin;

public class RssBroadcast extends ModulePlugin implements ModuleCommandExecutor {

	private File f;
	private FileConfiguration con;
	private CallRssFeedEvent event;
	
	@Override
	public void onEnable() {
		con = generateConfig();
		this.log("has been enabled!", ModuleLog.INFO);
		this.setExecutor("rssfeed", this);
		
		Bukkit.getPluginManager().registerEvents(new RssEventListener(), this.getEssentialsPlugin());
		
		if(con.getBoolean("rss.use-rss-broadcast")) {
			this.event = new CallRssFeedEvent(this, con);
			event.start();
		} else {
			this.log("rss broadcasts have been disabled in the configuration!", ModuleLog.INFO);
		}
	}

	@Override
	public void onDisable() {
		this.log("has been disabled!", ModuleLog.INFO);
	}
	
	/**
	 * generates config if not generated yet
	 * 
	 * @author xize
	 * @return FileConfiguration
	 */
	public FileConfiguration generateConfig() {
		this.f = new File(getDataFolder()+File.separator + "config.yml");
		if(!f.exists()) {
			this.log("no configuration found for RssBroadcast generating one now!", ModuleLog.INFO);
			FileConfiguration con = YamlConfiguration.loadConfiguration(f);
			con.set("rss.use-rss-broadcast", true);
			con.set("rss.use-rss-url", "https://mojang.com/feed/");
			con.set("rss.debug", false);
			try {
				con.save(f);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return con;
		}
		FileConfiguration con = YamlConfiguration.loadConfiguration(f);
		return con;
	}

	@Override
	public boolean execute(CommandSender sender, String cmd, String[] args) {
		if(cmd.equalsIgnoreCase("rssfeed")) {
			if(sender.hasPermission("rssbroadcast.use")) {
				if(args.length == 0) {
					sender.sendMessage(ChatColor.GOLD + ".oO___[RssFeed]___Oo.");
					sender.sendMessage(ChatColor.RED + "Admin: "+ChatColor.GRAY + "/rssfeed start : "+ChatColor.WHITE + "starts the rss broadcast");
					sender.sendMessage(ChatColor.RED + "Admin: "+ChatColor.GRAY + "/rssfeed stop : "+ChatColor.WHITE + "stops the rss broadcast");
					sender.sendMessage(ChatColor.RED + "Admin: "+ChatColor.GRAY + "/rssfeed reload : "+ChatColor.WHITE + "reload the configuration");
				} else if(args.length == 1) {
					if(args[0].equalsIgnoreCase("start")) {
						broadcast(ChatColor.DARK_GREEN + "[RssFeed]: "+ChatColor.GRAY + "broadcasts have been started!", "rssbroadcast.use");
						con.set("rss.use-rss-broadcast", true);
						event.start();
						try {
							con.save(f);
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					} else if(args[0].equalsIgnoreCase("stop")) {
						broadcast(ChatColor.DARK_GREEN + "[RssFeed]: "+ChatColor.GRAY + "broadcasts have been stopped!", "rssbroadcast.use");
						con.set("rss.use-rss-broadcast", false);
						try {
							con.save(f);
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						event.stop();
					} else if(args[0].equalsIgnoreCase("help")) {
						sender.sendMessage(ChatColor.GOLD + ".oO___[RssFeed]___Oo.");
						sender.sendMessage(ChatColor.RED + "Admin: "+ChatColor.GRAY + "/rssfeed start : "+ChatColor.WHITE + "starts the rss broadcast");
						sender.sendMessage(ChatColor.RED + "Admin: "+ChatColor.GRAY + "/rssfeed stop : "+ChatColor.WHITE + "stops the rss broadcast");
						sender.sendMessage(ChatColor.RED + "Admin: "+ChatColor.GRAY + "/rssfeed reload : "+ChatColor.WHITE + "reload the configuration");
					} else if(args[0].equalsIgnoreCase("reload")) {
						sender.sendMessage(ChatColor.DARK_GREEN + "[RssFeed]: "+ChatColor.GRAY+"reloading RssFeed!");
						event.stop();
						this.con = YamlConfiguration.loadConfiguration(f);
						event = new CallRssFeedEvent(this, con);
						event.start();
						sender.sendMessage(ChatColor.DARK_GREEN + "[RssFeed]: "+ChatColor.GRAY+"done reload RssFeed!");
					}
				}
			} else {
				sender.sendMessage(ChatColor.RED + "you are not allowed to use this command!");
			}
		}
		return false;
	}
	
	private void broadcast(String message, String permission) {
		for(Player p : Bukkit.getOnlinePlayers()) {
			if(p.hasPermission(permission)) {
				p.sendMessage(message);
			}
		}
	}

}
