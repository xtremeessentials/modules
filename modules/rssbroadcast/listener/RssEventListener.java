package modules.rssbroadcast.listener;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import modules.rssbroadcast.RssFeed;
import modules.rssbroadcast.event.RssFeedEvent;

public class RssEventListener implements Listener {

	@EventHandler
	public void onRss(RssFeedEvent e) {
		RssFeed rss = e.getRssFeed();
		Bukkit.broadcastMessage(ChatColor.DARK_GREEN + "====[ !NEWS! ]====");
		Bukkit.broadcastMessage(ChatColor.RED + rss.getAuthor() +ChatColor.GRAY+ " has posted new content:");
		Bukkit.broadcastMessage(ChatColor.RED+ "title: "+ChatColor.GRAY+rss.getTitle());
		Bukkit.broadcastMessage(ChatColor.RED + "url: "+ChatColor.GRAY+rss.getLink());
		Bukkit.broadcastMessage(ChatColor.DARK_GREEN + "====[ !END NEWS! ]====");
	}
	
}
